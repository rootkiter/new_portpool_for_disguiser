#!/bin/python
###############################################
# File Name : parser_from_litao.py
#    Author : rootkiter
#    E-mail : rootkiter@rootkiter.com
#   Created : 2018-06-04 15:05:17 CST
###############################################

import sys
from udp_mapbuilder import *
input_from_litao = 'tcpstream.txt'

def parse_from_litao(file_path):
    mapper = The_Mapper()
    with open(file_path) as f:
        for line in f:
            items = line.strip().replace("|"," ").split()
            if(len(items) != 3):
                continue
            if(items[0] == 'TCP'):
                mapper.tcp_add(items[1],items[2])
            elif(items[0] == 'UDP'):
                mapper.udp_add(items[1],items[2])
    mapper.tcp_dump()
    mapper.udp_dump()


if __name__=='__main__':
    parse_from_litao(input_from_litao)
