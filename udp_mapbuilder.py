#!/bin/python
###############################################
# File Name : udp_mapbuilder.py
#    Author : rootkiter
#    E-mail : rootkiter@rootkiter.com
#   Created : 2018-06-01 17:06:53 CST
###############################################

filepath = "./udp_amp.txt"

import os,time,json
out_basedir = os.path.dirname(os.path.abspath(__file__))
out_portpool= os.path.join(out_basedir,'portpool')
'''
sdanf -t amp_flood_pivot -l 1000 | awk '{print($3" "$5)}' | grep -v '-' |grep -v "123"| sort -u >> udp_amp.txt && cat udp_amp.txt | wc -l
'''

class The_Mapper:
    def __init__(self):
        self.udp_mapper = {}
        self.tcp_mapper = {}

    def tcp_add(self,port,ip):
        if(port not in self.tcp_mapper):
            self.tcp_mapper[port] = []
        if(ip not in self.tcp_mapper[port]):
            self.tcp_mapper[port].append(ip)

    def tcp_dump(self):
        if (not os.path.exists(out_portpool)):
            os.makedirs(out_portpool)
        for port in self.tcp_mapper:
            filename = 'tcp_%s.json' % port
            fpath = os.path.join(out_portpool,filename)
            # {"tmplist": [], "timestamp": 1523866949.150794, "worklist": [], "tmplen": "307", "time": "2018-04-16 08:04:29 CST"}
            res = {}    
            res["tmplist"] = self.tcp_mapper[port][:100]
            res["worklist"] = []
            res["tmplen"] = str(len(res['tmplist']))
            timestamp = time.time()
            res['timestamp'] = timestamp
            res["time"]   = time.strftime("%Y-%m-%d %H:%m:%S %Z", time.gmtime(timestamp))
            if(res['tmplen'] <= 0):
                continue
            with open(fpath,'w') as f:
                f.write(json.dumps(res)+"\n")
        

    def udp_add(self,port,ip):
        if(port not in self.udp_mapper):
            self.udp_mapper[port] = []
        if(ip not in self.udp_mapper[port]):
            self.udp_mapper[port].append(ip)

    def udp_dump(self):
        if (not os.path.exists(out_portpool)):
            os.makedirs(out_portpool)
        for port in self.udp_mapper:
            filename = 'udp_%s.json' % port
            fpath = os.path.join(out_portpool,filename)
            # {"tmplist": [], "timestamp": 1523866949.150794, "worklist": [], "tmplen": "307", "time": "2018-04-16 08:04:29 CST"}
            res = {}    
            res["tmplist"] = self.udp_mapper[port][:100]
            res["worklist"] = []
            res["tmplen"] = str(len(res['tmplist']))
            timestamp = time.time()
            res['timestamp'] = timestamp
            res["time"]   = time.strftime("%Y-%m-%d %H:%m:%S %Z", time.gmtime(timestamp))
            if(res['tmplen'] <= 0):
                continue
            with open(fpath,'w') as f:
                f.write(json.dumps(res)+"\n")


    def __str__(self):
        res = ""
        for port in self.udp_mapper:
            res += "%8s : %d\n" % (port,len(self.udp_mapper[port]))
        return res

import sys
if __name__=='__main__':
    #print "Hello World ! "
    #print sys.argv
    mapp = The_Mapper()
    with open (filepath,'r') as f:
        for line in f:
            ip,port = line.split()
            #print ip,port
            mapp.udp_add(port,ip)
    #print mapp.mapper
    mapp.udp_dump()
    print mapp
